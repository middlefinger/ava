<?php
// ensure this file is being included by a parent file
(defined('_VALID_MOS') OR defined('_JEXEC')) or die('Direct Access to this location is not allowed.');
/*
*
* Template for report to comment message
*
*/
class jtt_tpl_email_report extends JoomlaTuneTemplate
{
	function render() 
	{
		$comment = $this->getVar('comment');
		$object_link =  $this->getVar('comment-object_link');
		$name = $this->getVar('report-name');
		$reason = $this->getVar('report-reason');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta content="text/html; charset=<?php echo $this->getVar('charset'); ?>" http-equiv="content-type" />
  <meta name="Generator" content="JComments" />
</head>
<body>
<?php echo JText::sprintf('REPORT_MESSAGE', $comment->author, $name); ?>
<br />
<br />
<a href="<?php echo $object_link ?>#comment-<?php echo $comment->id; ?>" target="_blank"><?php echo $object_link ?></a>
<br />
<br />
<?php
		if ($reason != '') {
?>
<?php echo JText::_('REPORT_REASON'); ?>:<br /><?php echo $reason; ?>
<?php
		}

		if ($this->getVar('quick-moderation') == 1) {
			$commands = array();
			if ($comment->published == 0) {
				$commands[] = $this->getCmdLink('publish', JText::_('Publish'), $comment);
			} else {
				$commands[] = $this->getCmdLink('unpublish', JText::_('Unpublish'), $comment);
			}
			$commands[] = $this->getCmdLink('delete', JText::_('Delete'), $comment);

			if (count($commands)) {
?>
<br />
<br />
<?php	
				echo JText::_('Quick moderation') . ' ' . implode(' | ', $commands);
			}
		}
?>
</body>
</html>
<?php
	}

	function getCmdLink($cmd, $title, $comment)
	{
		$link = JCommentsFactory::getCmdLink($cmd, $comment->id);
		return '<a href="' . $link . '" title="' . $title . '" target="_blank">' . $title . '</a>';
	}
}
?>